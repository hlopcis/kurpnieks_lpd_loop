﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerDeath : MonoBehaviour
{

    private Scene scene;
    public int HP=50;
    public Text remaining;

    void Start()
    {
        scene = SceneManager.GetActiveScene();
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            HP = HP - 5;
            remaining.text = HP.ToString();
            if (HP <= 0)
            {
                Application.LoadLevel(scene.name);
            }
        }
    }


}
