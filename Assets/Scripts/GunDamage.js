﻿var DamagePerShot : int = 5;
var TargetDistance : float;
var Range : float=15;

function Update(){

    if(Input.GetButtonDown("Fire1")){
        var Shot : RaycastHit;
        if(Physics.Raycast (transform.position, transform.TransformDirection(Vector3.forward), Shot)){
            TargetDistance = Shot.distance;
            if(TargetDistance < Range){
                Shot.transform.SendMessage("DeductPoints", DamagePerShot);
            }
        }
    }
        

}