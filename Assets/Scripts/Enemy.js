﻿var HP : int = 10;
var skeleton : GameObject;

function DeductPoints(DamagePerShot : int) {
    HP -= DamagePerShot;
}

function Update(){
    if(HP <= 0) {
        skeleton.GetComponent.<AIMove>().Move = 0;
        this.GetComponent.<Animator>().Play("Death");
        Despawn();
        }
}

function Despawn(){    
    yield WaitForSeconds(2);
    Destroy(gameObject);
}