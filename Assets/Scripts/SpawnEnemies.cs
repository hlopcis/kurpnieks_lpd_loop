﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnEnemies : MonoBehaviour {

    public GameObject SpawnTrigger;
    public GameObject DespawnTrigger;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            SpawnTrigger.SetActive(true);
            DespawnTrigger.SetActive(false);
        }
    }
}
